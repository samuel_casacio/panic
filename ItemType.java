// Samuel Casacio
// CPSC 881 Geometric Modeling
// ItemType
// dealing with java enums :(

// enum for implemented curve types
enum ItemType {
  BUTTON(0), SLIDER(1), DATA(2);
  int value;
  
  ItemType(int value){
    this.value = value;
  }
  
  boolean equals(CurveType rhs){
    return this.value == rhs.value;
  }
};
