// Samuel Casacio
// CPSC 881 Geometric Modeling
// GetCurve
// draw a curve based on data

// linear interpolate, herp derp
void LERP(Point v1, Point v2, Point out, float t){
  out.x = (1.0 - t) * v1.x + t * v2.x;
  out.y = (1.0 - t) * v1.y + t * v2.y;
}

// reduce the array to an interpolated point on the curve
void dcReduce(PointData data, float t){

  Point v1, v2;

  // reduce the size of the array until the last item is a point on the curve
  while(data.size() > 1){
    
    // reduce the size of the data through interpolation...
    for(int i = 1; i < data.size(); i++){
      v1 = data.get(i-1);
      v2 = data.get(i);
      LERP(v1, v2, v1, t);
    }
    data.remove(data.size()-1);
  }
}

PointData deCasteljau(PointData data, int iter){
  float dt = 1.0 / float(iter);
  PointData curve = new PointData();
  curve.add(new Point(data.get(0)));
  for(float t = dt; t <= 1.0; t+=dt){
    PointData clone = data.deepCopy();
    dcReduce(clone, t);
    curve.add(new Point(clone.get(0)));
  }
//  curve.add(new Point(data.get(data.size()-1)));
  
  return curve;
}

PointData recurseSD(PointData p, PointData L, PointData R, float t){
  if(p.size() == 1){
    PointData ret = pdConcat(L, pdConcat(p, R));
    return ret;
  }
  L.add(new Point(p.first()));
  if(R.size() > 0) R.add(new Point(p.last()), 0);
  else R.add(new Point(p.last()));
  PointData pp = new PointData();
  for(int i = 0; i < p.size() - 1; i++){
    Point temp = new Point();
    LERP(p.get(i), p.get(i+1), temp, t);
    pp.add(temp);
  }

  return recurseSD(pp, L, R, t);
  
}

// destroys data, pass copy if you want to keep it
PointData bezierSD(PointData data, float t, int div){
  
  int size = data.size();
  PointData ret;
  PointData L = new PointData();  // should be size n at end
  PointData R = new PointData();  // should be size n at end
  
  if(div == 1){
    ret = recurseSD(data, L, R, t);
  }
  else{
    div--;
    ret = recurseSD(data, L, R, t);
    L.add(new Point(ret.get(size - 1)));
    R.add(new Point(ret.get(size - 1)), 0);
    L = bezierSD(L.deepCopy(), t, div);
    R = bezierSD(R.deepCopy(), t, div);
    R.remove(0);
    ret = pdConcat(L, R);
  }
  return ret;
}

float deBoor(ArrayList<Float> knots, int i, int d, float t){
  
  // base case, d = 0
  float ti = knots.get(i).floatValue();
  float tii = knots.get(i+1).floatValue();
  if(d == 1){
    if(ti <= t && t < tii) return 1.0;
    return 0.0;
  }
  
  float tid = knots.get(i+d).floatValue();
  float tidd = knots.get(i+d-1).floatValue();
  
  float n = 0.0;
  n += ( deBoor(knots, i, d - 1, t) * ((t - ti) / (tidd - ti + 0.00001)) );
  n += ( deBoor(knots, i + 1, d - 1, t) * ((tid - t) / (tid - tii + 0.00001)) );

  return n;
}

PointData bSpline(PointData data, int D, int samples){
  
  PointData ret = new PointData();
  
  // create the list of knots
  int n = data.size() - 1;
  int T = n + D + 1;  // degree
  ArrayList<Float> knots = new ArrayList<Float>();
  for(int i = 0; i < T; i++){
    if(i < D) knots.add(new Float(0));
    else if(D <= i && i <= n) knots.add(new Float(i - D + 1));
    else if(n < i && i <= n + D) knots.add(new Float(n - D + 2));
  }
  
  // sample a bunch of times over the spline
  float dt = float(n - D + 2) / float(samples-1);
  boolean breaka = false;
  float t = 0.0;
  while(!breaka){
    Point newp = new Point(0, 0);
    breaka = false;
    if(t >= float(n - D + 2)){
       t = float(n - D + 2) - 0.000001;
       breaka = true;
    }
    for(int i = 0; i <= n; i++){
      float alpha = deBoor(knots, i, D, t);
      Point pi = data.get(i);
      newp.x += pi.x * alpha;
      newp.y += pi.y * alpha;
    }
    t += dt;
    ret.add(newp);
  }
  
  return ret;
}
