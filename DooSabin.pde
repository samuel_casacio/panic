// Samuel Casacio
// CPSC 881 Geometric Modeling
// DooSabin
// Doo-Sabin subdivision

ArrayList<Vertex> subdivideFace(Mesh surface, int face){

  Tuple f = surface.faces.get(face);
  ArrayList<Vertex> verts = new ArrayList<Vertex>(f.size());
  
  // get all the vertices for the face
//  println("getting the vertices for face " + str(face));
  for(Integer i : f.verts){
//    println("adding vertex " + i.toString());
    verts.add(surface.getVertex(i));
  }

  // compute the face centroid
  Vertex c = surface.faceCentroid(face);
//  println("generating face centroid " + c.toString());
  
  // compute all edge centroids
  ArrayList<Integer> edges = surface.getEdges(f);
  ArrayList<Vertex> edgeCentroids = new ArrayList<Vertex>(edges.size());
  
//  println("getting edge centroids " + str(edges.size()));
  for(Integer i : edges){
    Tuple edge = surface.getEdge(i);
    Vertex ec = surface.edgeCentroid(i);
    edgeCentroids.add(ec);
//    println("edge " + edge.toString() + " centroid " + ec.toString());
  }
  
//  println("number of edge points: " + str(edgeCentroids.size()));
  
  // subdivide the face
//  println("subdividing the face");
  
  ArrayList<Vertex> ret = new ArrayList<Vertex>(verts.size());
  for(int i = 0; i < verts.size(); i++){

    int e2 = i - 1;
    if(e2 == -1){
      e2 = verts.size() - 1;
    }

//    println("V" + str(i) + " ve" + str(i) + " ve" + str(e2));
    
    Vertex v = verts.get(i);
    Vertex ve1 = edgeCentroids.get(i);
    Vertex ve2 = edgeCentroids.get(e2);
   
    Vertex vf = new Vertex();
    vf.x = 0.25 * (c.x + ve1.x + ve2.x + v.x);
    vf.y = 0.25 * (c.y + ve1.y + ve2.y + v.y);
    vf.z = 0.25 * (c.z + ve1.z + ve2.z + v.z);
    ret.add(vf);
    
//    println("new vertex: " + vf.toString());
  }
  
  return ret;
}

Mesh dooSabinRecurse(Mesh surface){
  
  surface.buildTables();
  
  Mesh subMesh = new Mesh();
  
  // generate subdivided points
//  println("getting subdivided faces");
  ArrayList<ArrayList<Vertex>> subVerts = new ArrayList<ArrayList<Vertex>>(surface.numFaces());
  for(int i = 0; i < surface.numFaces(); i++){
    subVerts.add(subdivideFace(surface, i));
  }
  
  // add subdivided points to new mesh
//  println("adding subdivided face vertices to mesh");
  for(int subFace = 0; subFace < subVerts.size(); subFace++){
  
    ArrayList<Vertex> verts = subVerts.get(subFace);
    
//    println("face " + str(subFace) + " " + surface.getFace(subFace).toString() + " new verts: ");
    
    for(Vertex v : verts){
    
//      println(str(subMesh.numVerts()));
    
      subMesh.addVertex(v);
    }
  }

  // generate face faces
//  println("making face faces");
  if(subVerts.size() > 0){
    int v = 0;
    for(ArrayList<Vertex> subFace : subVerts){

      Tuple f = new Tuple();
      for(int i = 0; i < subFace.size(); i++){
        f.add(v);
        v++;
      }
      
      subMesh.addFace(f);
    }
  }

  // generate edge faces
//  println("making edge faces");
  if(subVerts.size() > 0){
    
    for(Tuple edge : surface.getEdges()){

      // see if edge belongs to 2 faces
      ArrayList<Integer> faces = surface.getFaces(edge);

      if(faces.size() == 2){
        
        // add an edge face
        
        // get the face corresponding to the sub-face (1:1)
        Tuple subFace0 = subMesh.getFace(faces.get(0));
        Tuple subFace1 = subMesh.getFace(faces.get(1));
        
        // vertices of the edge
        Vertex ev0 = surface.getVertex(edge.get(0));
        Vertex ev1 = surface.getVertex(edge.get(1));
        
        // vertices of corresponding face faces
        int v0 = getClosestVertex(subMesh, subFace0, ev0);
        int v1 = getClosestVertex(subMesh, subFace0, ev1);
        int v2 = getClosestVertex(subMesh, subFace1, ev1);
        int v3 = getClosestVertex(subMesh, subFace1, ev0);
        
        Tuple f = new Tuple();
        f.add(v0);
        f.add(v1);
        f.add(v2);
        f.add(v3);
        subMesh.addFace(f);
      }
    }
  }
  
//  println("making vertex faces");
  if(subVerts.size() > 0){
  
    // build the current edge tables
    subMesh.buildTables();
    
    ArrayList<Tuple> newfaces = new ArrayList<Tuple>();

    for(int v = 0; v < surface.numVerts(); v++){
      
      Vertex vert = surface.getVertex(v);
      
      // check to see if we have a vert that is high valence (>2)
      ArrayList<Integer> faces = surface.getFaces(vert);
     
      // does this vertex have a face?
      if(faces.size() > 2){
        
        Tuple f = new Tuple();
        
        // get all the vertices belonging to this new face
        ArrayList<Integer> subVs = new ArrayList<Integer>(faces.size());
        for(int fi = 0; fi < faces.size(); fi++){

          int face = faces.get(fi);
//          println("face: " + str(face));
          Tuple subFace = subMesh.getFace(face);
          int subV = getClosestVertex(subMesh, subFace, vert);
          subVs.add(subV);
        }
        
        // start adding verts in edge order
        HashMap<Integer, Tuple> addEdges = new HashMap<Integer, Tuple>();
        int curr = 0;
        
        for(int i = 0; i < subVs.size(); i++){
          f.add(subVs.get(curr));

          // find an edge that doesnt already exist
          boolean found = false;
          int counter = (curr + 1) % subVs.size();
          int currV = subVs.get(curr);
          
//          int prev = curr;
          while(!found){
          
            if(counter == curr){
//              println(str(subVs.size()));
              curr = subVs.size() - 1;
//              curr = prev;
              found = true;
//              println("stuck on edge " + str(curr) + " " + str(counter));
//              for(Integer testing : subVs){
//                println(testing.toString());
//              }
            }
          
            // candidate edge
            int candV = subVs.get(counter);
            
            // oh hey, this can has edge
            if(subMesh.hasEdge(currV, candV)){
            
              int foundedge = subMesh.getEdge(currV, candV);
//              println("candidate edge: " + subMesh.getEdge(foundedge).toString());
              
              // dont already has edge
              if(!addEdges.containsKey(foundedge)){
                
//                println("new edge, adding");
                // new edge found!
                found = true;
                addEdges.put(foundedge, subMesh.getEdge(foundedge));
                curr = counter;
//                f.add(subVs.get(curr));
              }
            }
            
//            prev = counter;
            counter = (counter + 1) % subVs.size();
//            println("counter: " + str(counter));
          }
        }
        
//        println("adding face: " + f.toString());
        
        newfaces.add(f);
      }
    }
    
//    println("mesh has " + str(newfaces.size()) + " vert faces");
    
    for(Tuple f : newfaces){
//      println(f.toString());
      subMesh.addFace(f);
    }
  }
  
  return subMesh;
}

Mesh dooSabin(Mesh surface, int iterations){
  
  if(surface.numFaces() == 0){
    return null;
  }

  Mesh subMesh = dooSabinRecurse(surface);
  
  for(int i = 1; i < iterations; i++){
    subMesh = dooSabinRecurse(subMesh);
  }
  
  return subMesh;
}

