// Samuel Casacio
// CPSC 881 Geometric Modeling
// LoadObj
// quick obj loader

Mesh loadObj(String filename){

  Mesh mesh = new Mesh();
  
  String[] file = loadStrings(filename);
  
  // does the file exist?
  if(file == null || file.length == 0){
    return null;
  }

  for(String line : file){

    // do nothing for blank space
    if(line.length() == 0){
      // empty
    }
    
    // we have a vertex
    if(line.indexOf("v") == 0){
      String[] splits = line.split(" ");
      float x = Float.parseFloat(splits[1]);
      float y = Float.parseFloat(splits[2]);
      float z = Float.parseFloat(splits[3]);
      Vertex v = new Vertex(x, y, z);
      mesh.addVertex(v);
    }
    
    // we have a face
    else if(line.indexOf("f") == 0){
      String[] splits = line.split(" ");
      Tuple face = new Tuple();
      
      for(int i = 1; i < splits.length; i++){
        
        int v = 0;

        if(splits[i].contains("/")){
          String[] in = splits[i].split("/");
          v = Integer.parseInt(in[0]);
        }
        
        else{
          v = Integer.parseInt(splits[i]);
        }

        v--;
        face.add(v);
      }
      mesh.addFace(face);
    }
    
    // dont know what, ignore line
    else{
      // empty
    }
  }
  
  println("Loaded Vertices " + str(mesh.numVerts()) + " Faces: " + str(mesh.numFaces()));
  
  return mesh;
}
