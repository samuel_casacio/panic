// Samuel Casacio
// CPSC 881 Geometric Modeling
// Mesh
// Simple meshing library

class Tuple{
  ArrayList<Integer> verts;
//  Vertex centroid;  
  
  Tuple(){
    verts = new ArrayList<Integer>();
//    centroid = null;
  }

  Tuple(ArrayList<Integer> v){
    verts = v;
  }

  void assign(ArrayList<Integer> v){
    verts = v;
  }

  void assign(Tuple f){
    verts = f.verts;
  }
  
  Tuple deepCopy(){
    ArrayList<Integer> v = new ArrayList<Integer>(verts.size());
    for(Integer i : verts){
      v.add(i);
    }
    return (new Tuple(v));
  }

  void add(Integer v){
    verts.add(v);
//    centroid = null;
  }

  int get(Integer i){
    return verts.get(i);
  }

  int size(){
    return verts.size();
  }
  
  String toString(){
    String ret = "[";
    
    for(int i = 0; i < verts.size(); i++){
      ret += verts.get(i).toString();
      if(i < verts.size() - 1){
        ret += ", ";
      }
    }
    
    ret += "]";
    return ret;
  }
  
  boolean contains(int vert){
    
    for(Integer i : verts){
      if(vert == i){
        return true;
      }
    }
    
    return false;
  }
}

class Mesh{
  
  ArrayList<Vertex> verts;
  ArrayList<Tuple> faces;
  ArrayList<Tuple> edges;
  HashMap<String, Integer> edgeLookUp;
  HashMap<Vertex, ArrayList<Integer>> vertEdges;
  HashMap<Vertex, ArrayList<Integer>> vertFaces;
  HashMap<Tuple, ArrayList<Integer>> edgeFaces;
  HashMap<Tuple, ArrayList<Integer>> faceEdges;
  
  boolean isPatch;
  int width;
  int height;
  boolean wClosed;
  boolean hClosed;
  boolean dirty;
  
  Mesh(){
    verts = new ArrayList<Vertex>();
    faces = new ArrayList<Tuple>();
    edges = null;
    edgeLookUp = null;
    vertEdges = null;
    vertFaces = null;
    edgeFaces = null;
    faceEdges = null;
    isPatch = false;
    width = 0;
    height = 0;
    wClosed = false;
    hClosed = false;
    dirty = false;
  }

  void buildTables(){
  
    if(dirty == false){
      return;
    }
    
    dirty = false;
  
    // class variables
    edges = new ArrayList<Tuple>();
    vertEdges = new HashMap<Vertex, ArrayList<Integer>>();
    vertFaces = new HashMap<Vertex, ArrayList<Integer>>();
    edgeFaces = new HashMap<Tuple, ArrayList<Integer>>();
    faceEdges = new HashMap<Tuple, ArrayList<Integer>>();
    edgeLookUp = new HashMap<String, Integer>();
    
    // allocate ALL THE THINGS!!!!!!!
    for(Vertex v : verts){
      vertEdges.put(v, new ArrayList<Integer>());
      vertFaces.put(v, new ArrayList<Integer>());
    }
    
    for(Tuple f : faces){
      faceEdges.put(f, new ArrayList<Integer>());
    }
    
    for(int face = 0; face < faces.size(); face++){
      
      Tuple f = faces.get(face);
      
      for(int v = 0; v < f.size(); v++){
      
        // verts
        int vi = f.get(v);
        int vii = f.get((v + 1) % f.size());
        
        // add face to verts table
        vertFaces.get(verts.get(vi)).add(face);
        
        // edge key
        String key = str(min(vi, vii)) + "," + str(max(vi, vii));
        
        // see if the edge exist
        if(!edgeLookUp.containsKey(key)){

          // add new edge to table
          Tuple edge = new Tuple();
          edge.add(vi);
          edge.add(vii);
          edgeLookUp.put(key, edges.size());
          
          Vertex verti = verts.get(vi);
          Vertex vertii = verts.get(vii);
          
          // add new edge to vert tables
          vertEdges.get(verti).add(edges.size());
          vertEdges.get(vertii).add(edges.size());
          
          // add new edge to list
          edges.add(edge);
        }

        // add edge to face table
        ArrayList<Integer> fPtr = faceEdges.get(f);
        fPtr.add(edgeLookUp.get(key));
        
        // add face to edge table
        Tuple edge = edges.get(edgeLookUp.get(key));
        if(!edgeFaces.containsKey(edge)){
          edgeFaces.put(edge, new ArrayList<Integer>());
        }
        
        edgeFaces.get(edge).add(face);
      }
    }
  }
  
  Mesh deepCopy(){
    ArrayList<Vertex> vs = new ArrayList<Vertex>(verts.size());
    ArrayList<Tuple> fs = new ArrayList<Tuple>(faces.size());
    
    for(int i = 0; i < verts.size(); i++){
      vs.add(verts.get(i).deepCopy());
    }
    for(int i = 0; i < faces.size(); i++){
      fs.add(faces.get(i).deepCopy());
    }
    
    Mesh mesh = new Mesh();
    mesh.verts = vs;
    mesh.faces = fs;
    mesh.width = width;
    mesh.height = height;
    mesh.isPatch = isPatch;
    mesh.wClosed = wClosed;
    mesh.hClosed = hClosed;
    mesh.dirty = true;
    return mesh;
  }
  
  void clear(){
    verts.clear();
    faces.clear();
    edges = null;
    edgeLookUp = null;
    vertEdges = null;
    vertFaces = null;
    edgeFaces = null;
    faceEdges = null;

    isPatch = false;
    width = 0;
    height = 0;
    wClosed = false;
    hClosed = false;
  }

  ArrayList<Integer> getEdges(Vertex v){
    if(dirty){
      buildTables();
    }
    
    return vertEdges.get(v);
  }
  
  ArrayList<Integer> getEdges(Tuple f){
    if(dirty){
      buildTables();
    }
    return faceEdges.get(f);
  }
  
  ArrayList<Tuple> getEdges(){
    if(dirty){
      buildTables();
    }
    return edges;
  }
  
  ArrayList<Integer> getFaces(Vertex v){
    if(dirty){
      buildTables();
    }
    return vertFaces.get(v);
  }
  
  ArrayList<Integer> getFaces(Tuple e){
    if(dirty){
      buildTables();
    }
    return edgeFaces.get(e);
  }
  
  Tuple getEdge(int i){
    if(dirty){
      buildTables();
    }
    
    Tuple edge = null;
    
    if(i < edges.size()){
      edge = edges.get(i);
    }
    
    return edge;  
  }
  
  // get an edge from 2 vertices
  int getEdge(int v1, int v2){
    if(dirty){
      buildTables();
    }

    String key = str(min(v1, v2)) + "," + str(max(v1, v2));
    return edgeLookUp.get(key);
  }
  
  boolean hasEdge(int v1, int v2){
    if(dirty){
      buildTables();
    }

    String key = str(min(v1, v2)) + "," + str(max(v1, v2));
    return edgeLookUp.containsKey(key);
  }
  
  int numEdges(){
    if(dirty){
      buildTables();
    }
    
    return edges.size();  
  }
  
  void addVertex(Vertex v){
    verts.add(v);
  }
 
  void replaceVerts(ArrayList<Vertex> verts){
    this.verts = verts;
  }
  
  Vertex getVertex(int index){
    return verts.get(index);
  }

  ArrayList<Vertex> getVerts(){
    return verts;
  }

  int numVerts(){
    return verts.size();
  }
  
  Tuple getFace(int index){
    return faces.get(index);
  }
  
  void addFace(Tuple f){
    faces.add(f);
    dirty = true;
  }

  void replaceFaces(ArrayList<Tuple> faces){
    this.faces = faces;
    dirty = true;
  }
  
  ArrayList<Tuple> getFaces(){
    return faces;
  }
  
  int numFaces(){
    return faces.size();
  }
  
  boolean isTriangleMesh(){

    if(faces.size() == 0){
      return false;
    }
    
    for(Tuple f : faces){
      if(f.size() != 3){
        return false;
      }
    }
    
    return true;
  }
  
  Vertex faceCentroid(int index){

    if(faces.size() == 0){
      return null;
    }

    Tuple face = faces.get(index);
/*
    if(face.centroid != null){
      return face.centroid;
    }
*/    
    Vertex cent = new Vertex();
    
    for(Integer i : face.verts){
      Vertex v = verts.get(i);
      cent.x += v.x;
      cent.y += v.y;
      cent.z += v.z;
    }
    
    float div = 1.0 / float(face.size());
    
    cent.x *= div;
    cent.y *= div;
    cent.z *= div;
    
//    face.centroid = cent;
    
    return cent;
  }
  
  Vertex edgeCentroid(int index){
    if(dirty == true){
      buildTables();
    }

    if(edges.size() == 0){
      return null;
    }
    
    Tuple edge = edges.get(index);
/*    
    if(edge.centroid != null){
      return edge.centroid;
    }
*/    
    Vertex cent = new Vertex();
    
    for(Integer i : edge.verts){
      Vertex v = verts.get(i);
      cent.x += v.x;
      cent.y += v.y;
      cent.z += v.z;
    }
    
    float div = 1.0 / float(edge.size());
    
    cent.x *= div;
    cent.y *= div;
    cent.z *= div;
    
//    edge.centroid = cent;
    
    return cent;
  }
  
  // return a list of vertices connected to the input
  ArrayList<Integer> oneRing(int index){
    
    if(dirty){
      buildTables();
    }
    
    Vertex v = verts.get(index);
    ArrayList<Integer> vedges = vertEdges.get(v);
    
    ArrayList<Integer> ring = new ArrayList<Integer>(vedges.size());
    
    for(Integer e : vedges){
      Tuple edge = edges.get(e);
      int opp = 0;
      
      if(edge.get(opp) == index){
        opp = 1;
      }
      
      ring.add(edge.get(opp));
    }
    
    return ring;
  }
}

// given a mesh, a face, and a vertex return the closest vertex in the face
int getClosestVertex(Mesh mesh, Tuple face, Vertex v){
  int closest = 0;
  float distance = Float.MAX_VALUE;
  
  for(int i = 0; i < face.size(); i++){
    Vertex vf = mesh.getVertex(face.verts.get(i));
    float length = v.sub(vf).length();
    if(length < distance){
      closest = face.verts.get(i);
      distance = length;
    }
  }
  
  return closest;
}
