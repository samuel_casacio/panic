// Samuel Casacio
// CPSC 881 Geometric Modeling
// Viewport
// A viewport

class Viewport{

  // viewport info
  Point LLC;
  Point URC;
  boolean show;
  
  Viewport(Point llc, Point urc){
    LLC = llc;
    URC = urc;
    show = false;
  }
  
  boolean isVisible(){
    return show;
  }
  
  void setVisibility(boolean show){
    this.show = show;
  }
  
  void draw(){

    if(!show) return;

    strokeWeight(1);
    stroke(0);
    fill(150);
    rect(LLC.x, LLC.y, URC.x, URC.y);
  }
  
  boolean event(){
    if(show && mouseX >= int(LLC.x) && mouseX <= int(URC.x) && mouseY >= int(LLC.y) && mouseY <= int(URC.y)){
      return true;
    }
    return false;
  }
  
  void handleMove(){
    // do nothing...
  }
  
  void handlePush(){
    // do nothing...
  }
  
  void handleDrag(){
    // do nothing...
  }
  
  void handleRelease(){
    // do nothing...
  }
}
