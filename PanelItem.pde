// Samuel Casacio
// CPSC 881 Geometric Modeling
// PanelItem
// Displays a label with a value

class PanelItem{

  boolean show;  // should this item be shown?
  String label;
  Point LLC;
  Point URC;
  Point localLC;
  Point localRC;
  float value;   // value

  // for UX
  boolean horizontal;
  
  PanelItem(String label, Point LLC, Point URC, boolean show, boolean horizontal){
    this.label = label;
    this.LLC = LLC;
    this.URC = URC;
    this.localLC = new Point(LLC);
    this.localRC = new Point(URC);
    this.show = show;
    this.horizontal = horizontal;
  }
  
  void setOffset(int offset){
    if(horizontal){
      localLC.x = LLC.x - float(offset) * 150.0;
      localRC.x = URC.x - float(offset) * 150.0;
    }
    else{
      localLC.y = LLC.y - float(offset) * 40.0;
      localRC.y = URC.y - float(offset) * 40.0;      
    }
  }
  
  void draw(){
    
    if(!show) return;
    
    int texts = 20;
    float pad = 30;
    noFill();
    stroke(0);
    textSize(texts);
    while(textWidth(label) + textWidth(str(value)) + pad * 2.0 > localRC.x - localLC.x){
      texts--;
      pad *= 0.75;
      textSize(texts);
    }
    rect(localLC.x, localRC.y, localRC.x - localLC.x, localLC.y - localRC.y);
    noStroke();
    fill(0);
    text(label, localLC.x + pad, localRC.y - 10);
    text(str(value), localLC.x + textWidth(label) + pad * 2, localRC.y - 10);
  }
  
  boolean isVisible(){
    return show;
  }
  
  void setVisibility(boolean show){
    this.show = show;
  }
  
  void set(float value){
    this.value = value;
  }
  
  float get(){
    return this.value;
  }
  
  boolean event(){
    return false;
  }
  
  void handlePush(){
    return;
  }
  
  void handleRelease(){
    return;
  }
  
  void handleDrag(){
    return;
  }
  
  void range(Point p){
    return;
  }
}
