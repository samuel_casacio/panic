// Samuel Casacio
// CPSC 881 Geometric Modeling
// Triangulate
// Triangulate an input mesh

void triangulate(Mesh mesh){
  
  ArrayList<Tuple> faces = new ArrayList<Tuple>();
  
  // iterate through each face in the mesh
  for(Tuple f : mesh.faces){
    
//    println("old face: " + f.toString());
    
    // if the face has more than 3 verts...
    if(f.size() > 3){

      // cycle through the face and create triangles
      for(int i = 2; i < f.size(); i++){
        Tuple fp = new Tuple();
        fp.add(f.get(0));
        fp.add(f.get(i));
        fp.add(f.get( (i - 1) ));
        faces.add(fp);
//        println("new face: " + fp.toString());
      }
    }
    
    // the current face is already a triangle
    else{
      faces.add(f);
    }
  }
  
  mesh.replaceFaces(faces);
}
