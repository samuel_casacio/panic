class Point{
  float x;
  float y;
  
  Point(){
    this.x = 0.0;
    this.y = 0.0;
  }
  
  Point(float x, float y){
    this.x = x;
    this.y = y;
  }
  
  Point(Point a){
    this.x = a.x;
    this.y = a.y;
  }
  
  void assign(Point a){
    this.x = a.x;
    this.y = a.y;
  }
  
  Point mult(float x){
    return new Point(this.x * x, this.y * x);
  }
  
  String toString(){
    String str = "[x, y]: [" + str(x) + ", " + str(y) + "]";
    return str;
  }
  
  boolean equals(Point p){
    if(this.x == p.x && this.y == p.y){
      return true;
    }
    
    return false;
  }
}
