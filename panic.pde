// Samuel Casacio
// CPSC 881 Geometric Modeling
// Panic
// simple curve plotting program

// right panel
Panel rightPanel;

// right panel curve data
PanelItem selectedX;
PanelItem selectedY;
PanelItem deleteP;
PanelItem bsSamples;
PanelItem subdivisions;
PanelItem iterations;
PanelItem order;
PanelItem drawControl;
PanelItem closeCurve;
PanelItem clearCurve;

// right panel surface data
PanelItem sliceNum;
PanelItem sliceSize;
PanelItem angleRot;
PanelItem wireframe;
PanelItem meshOut;
PanelItem meshIn;

// right panel surface subdivision
PanelItem bezierSubdivision;
PanelItem bsplineSubdivision;
PanelItem subWidth;
PanelItem subHeight;
PanelItem dooSabinSubdivision;
PanelItem loopSubdivision;
PanelItem subIterations;
PanelItem subdivide;
PanelItem original;
PanelItem triangulate;

// top panel
Panel topPanel;

// curve options
PanelItem bezierSub;
PanelItem bezierDCJ;
PanelItem bspline;

// surface options
PanelItem extrusion;
PanelItem rotation;
PanelItem sweep;

// render options
CurveCanvas xyCanvas;
CurveCanvas yzCanvas;
SurfaceView surfaceViewport;

// what is being drawn?
Viewport activeViewport;

PanelItem curveXY;
PanelItem curveYZ;
PanelItem surfaceItem;

// drawing information
boolean mouseDown;
PanelItem downItem;

boolean surfaceDirty;

// file output
boolean fileMode;
boolean inputFileMode;
String outputFile;
String[] objData;

void setup(){
  size(1024, 768, P3D);
 
  rightPanel = new Panel(new Point(800, height - 725), new Point(width - 1, height - 1), false);
  selectedX = rightPanel.newItem(ItemType.DATA, "Selected X:", true);
  selectedY = rightPanel.newItem(ItemType.DATA, "Selected Y:", true);
  deleteP = rightPanel.newItem(ItemType.BUTTON, "Delete Selected", true);
  
  iterations = rightPanel.newItem(ItemType.SLIDER, "Bezier Iterations", true);
  iterations.range(new Point(2, 25));
  iterations.set(2.0);

  subdivisions = rightPanel.newItem(ItemType.SLIDER, "Subdivisions", false);
  subdivisions.range(new Point(1, 10));
  subdivisions.set(1.0);
  
  bsSamples = rightPanel.newItem(ItemType.SLIDER, "B-Spline Samples", false);
  bsSamples.range(new Point(3, 100));
  bsSamples.set(3.0);
  
  order = rightPanel.newItem(ItemType.SLIDER, "Order", false);
  order.range(new Point(3, 3));
  order.set(3.0);
  
  drawControl = rightPanel.newItem(ItemType.BUTTON, "Draw Control Polygon", true);
  drawControl.set(1.0);
  closeCurve = rightPanel.newItem(ItemType.BUTTON, "Close Curve", true);
  clearCurve = rightPanel.newItem(ItemType.BUTTON, "Clear Curve", true);
  
  sliceNum = rightPanel.newItem(ItemType.SLIDER, "Number of Slices", false);
  sliceNum.range(new Point(2, 100));
  sliceNum.set(3.0);
  sliceSize = rightPanel.newItem(ItemType.SLIDER, "Size of Slice", false);
  sliceSize.range(new Point(1, 100));
  sliceSize.set(50.0);
  angleRot = rightPanel.newItem(ItemType.SLIDER, "Amount of Rotation", false);
  angleRot.range(new Point(0, 360));
  angleRot.set(0);
  wireframe = rightPanel.newItem(ItemType.BUTTON, "Wireframe Mode", false);
  meshOut = rightPanel.newItem(ItemType.BUTTON, "Output Obj", false);
  meshIn = rightPanel.newItem(ItemType.BUTTON, "Input Obj", false);

  // subdivision options
  bezierSubdivision = rightPanel.newItem(ItemType.BUTTON, "Bezier Subdivision", false);
  bezierSubdivision.set(1.0);
  bsplineSubdivision = rightPanel.newItem(ItemType.BUTTON, "B-Spline Subdivison", false);

  subWidth = rightPanel.newItem(ItemType.SLIDER, "Width Multiple", false);
  subWidth.range(new Point(2, 20));
  subWidth.set(2.0);
  subHeight = rightPanel.newItem(ItemType.SLIDER, "Height Multiple", false);
  subHeight.range(new Point(2, 20));
  subHeight.set(2.0);
  
  dooSabinSubdivision = rightPanel.newItem(ItemType.BUTTON, "Doo-Sabin Subdivision", false);
  loopSubdivision = rightPanel.newItem(ItemType.BUTTON, "Loop Subdivision", false);

  subIterations = rightPanel.newItem(ItemType.SLIDER, "Surface Sub Iterations", false);
  subIterations.range(new Point(1, 5));
  subIterations.set(1.0);

  subdivide = rightPanel.newItem(ItemType.BUTTON, "Subdivide", false);
  original = rightPanel.newItem(ItemType.BUTTON, "Show Original Mesh", false);
  original.set(1.0);
  triangulate = rightPanel.newItem(ItemType.BUTTON, "Triangulate", false);
  
  topPanel = new Panel(new Point(0, 0), new Point(width - 1, height - 725), true);

  // curve types
  bezierDCJ = topPanel.newItem(ItemType.BUTTON, "Bezier (De Casteljau)", true);
  bezierDCJ.set(1.0);
  bezierSub = topPanel.newItem(ItemType.BUTTON, "Bezier (Subdivision)", true);
  bspline = topPanel.newItem(ItemType.BUTTON, "B-Spline", true);
  
  // surface types
  extrusion = topPanel.newItem(ItemType.BUTTON, "Extrusion", false);
  extrusion.set(1.0);
  rotation = topPanel.newItem(ItemType.BUTTON, "Rotation", false);
  sweep = topPanel.newItem(ItemType.BUTTON, "Sweep", false);
 
  // canvas options
  curveXY = topPanel.newItem(ItemType.BUTTON, "XY Curve", true);
  curveXY.set(1.0);
  curveYZ = topPanel.newItem(ItemType.BUTTON, "YZ Curve", true);
  surfaceItem = topPanel.newItem(ItemType.BUTTON, "3D Surface", true);

  xyCanvas = new CurveCanvas(new Point(0, height - 725), new Point(800, height - 1));
  xyCanvas.setVisibility(true);
  xyCanvas.setIterations(2);
  yzCanvas = new CurveCanvas(new Point(0, height - 725), new Point(800, height - 1));
  yzCanvas.setVisibility(false);
  yzCanvas.setIterations(2);
  surfaceViewport = new SurfaceView(new Point(0, height - 725), new Point(800, height - 1));
  surfaceViewport.setVisibility(false);
  
  activeViewport = xyCanvas;
  
  downItem = null;
  mouseDown = false;
  surfaceDirty = true;
  outputFile = new String("../mesh.obj");
  fileMode = false;
  inputFileMode = false;
}

void draw(){
  smooth();
  
  if(fileMode){
    background(255);
    stroke(0);
    strokeWeight(10);
    String printit = "Enter File Name: " + outputFile;
    text(printit, float(width) * 0.5 - textWidth(printit) * 0.5, (height / 2));
    return;
  }
  
  // check to see if we need to dirty the surface (before draw resets)
  if( xyCanvas.isDirty() || ( (sweep.value == 1.0) && (yzCanvas.isDirty()) ) ){
    surfaceDirty = true;
  }
  
  if(activeViewport == surfaceViewport){
    updateSurface();
  }
  
  activeViewport.draw();
  rightPanel.draw();
  topPanel.draw();
}

void mouseMoved(){
  if(activeViewport.event()){
    activeViewport.handleMove();
  }
}

void disableCurveOptions(){
  bezierSub.setVisibility(false);
  bezierDCJ.setVisibility(false);
  bspline.setVisibility(false);
  selectedX.setVisibility(false);
  selectedY.setVisibility(false);
  deleteP.setVisibility(false);
  bsSamples.setVisibility(false);
  subdivisions.setVisibility(false);
  iterations.setVisibility(false);
  order.setVisibility(false);
  drawControl.setVisibility(false);
  closeCurve.setVisibility(false);
  clearCurve.setVisibility(false);
}

void disableSurfaceOptions(){
  extrusion.setVisibility(false);
  rotation.setVisibility(false);
  sweep.setVisibility(false);
  sliceNum.setVisibility(false);
  sliceSize.setVisibility(false);
  angleRot.setVisibility(false);
  wireframe.setVisibility(false);
  meshOut.setVisibility(false);
  meshIn.setVisibility(false);
  bezierSubdivision.setVisibility(false);
  bsplineSubdivision.setVisibility(false);
  dooSabinSubdivision.setVisibility(false);
  loopSubdivision.setVisibility(false);
  subWidth.setVisibility(false);
  subHeight.setVisibility(false);
  subIterations.setVisibility(false);
  subdivide.setVisibility(false);
  original.setVisibility(false);
  triangulate.setVisibility(false);
}

void setCurveOptions(PanelItem item){
  if(item.value == 0.0){
    bezierSub.set(0.0);
    bezierDCJ.set(0.0);
    bspline.set(0.0);
    surfaceItem.set(0.0);
    item.set(1.0);

    // disable all options not dealing with curves
    disableSurfaceOptions();
    
    selectedX.setVisibility(true);
    selectedY.setVisibility(true);
    deleteP.setVisibility(true);
    bsSamples.setVisibility(false);
    subdivisions.setVisibility(false);
    iterations.setVisibility(false);
    order.setVisibility(false);
    drawControl.setVisibility(true);
    closeCurve.setVisibility(true);
    clearCurve.setVisibility(true);

    // surface needs to be updated
    surfaceDirty = true;

    // set curve type to bezierSUB
    if(item == bezierSub){
      subdivisions.setVisibility(true);
      ((CurveCanvas)activeViewport).changeType(CurveType.BEZSUB);
    }
    else if(item == bezierDCJ){
      iterations.setVisibility(true);
      ((CurveCanvas)activeViewport).changeType(CurveType.BEZDCJ);
    }
    else if(item == bspline){
      bsSamples.setVisibility(true);
      order.setVisibility(true);
      ((CurveCanvas)activeViewport).changeType(CurveType.BS);
    }
  }
}

void disableViewports(){
  curveXY.set(0.0);
  curveYZ.set(0.0);
  surfaceItem.set(0.0);
  xyCanvas.setVisibility(false);
  yzCanvas.setVisibility(false);
  surfaceViewport.setVisibility(false);
}

void loadSurfaceOptions(PanelItem item){
  if(item.value == 0.0){
    
    // disable all viewports and curve options
    disableViewports();
    disableCurveOptions();
    disableSurfaceOptions();
    
    item.set(1.0);
    
    // top panel
    extrusion.setVisibility(true);
    rotation.setVisibility(true);
    sweep.setVisibility(true);

    // set the correct viewport 
    if(item == surfaceItem){
      activeViewport = surfaceViewport;
    }

    activeViewport.setVisibility(true);
    
    // right panel
    if(extrusion.value == 1.0){
      sliceNum.setVisibility(true);
      sliceSize.setVisibility(true);
      angleRot.setVisibility(false);
    }
    else if(rotation.value == 1.0){
      sliceNum.setVisibility(true);
      sliceSize.setVisibility(false);
      angleRot.setVisibility(true);          
    }
    wireframe.setVisibility(true);
    meshOut.setVisibility(true);
    meshIn.setVisibility(true);
    
    bezierSubdivision.setVisibility(true);
    bsplineSubdivision.setVisibility(true);
    dooSabinSubdivision.setVisibility(true);
    loopSubdivision.setVisibility(true);
    
    if(bezierSubdivision.value == 1.0 || bsplineSubdivision.value == 1.0){
      subWidth.setVisibility(true);
      subHeight.setVisibility(true);
    }
    else if(dooSabinSubdivision.value == 1.0 || loopSubdivision.value == 1.0){
      subIterations.setVisibility(true);
    }
    subdivide.setVisibility(true);
    original.setVisibility(true);
    triangulate.setVisibility(true);
  }
}

void loadCurveOptions(PanelItem item){
  if(item.value == 0.0){
    CurveCanvas cur;

    disableViewports();
    disableCurveOptions();
    disableSurfaceOptions();
    
    item.set(1.0);
    
    if(item == curveXY){
      cur = xyCanvas;
      activeViewport = xyCanvas;
      xyCanvas.setVisibility(true);
    }
    else{
      cur = yzCanvas;
      activeViewport = yzCanvas;
      yzCanvas.setVisibility(true);
    }
    
    bezierSub.setVisibility(true);
    bezierSub.set(0.0);

    bezierDCJ.setVisibility(true);
    bezierDCJ.set(0.0);

    bspline.setVisibility(true);
    bspline.set(0.0);

    closeCurve.set(cur.getClosed() == false ? 0.0 : 1.0);
    Point p = cur.getSelected();
    selectedX.set(p.x);
    selectedY.set(p.y);
    
    order.set(cur.order);
    order.range(new Point(3, max(cur.size(), 3)));
    
    iterations.set(cur.iterations);
    
    subdivisions.set(cur.subdivisions);
    
    bsSamples.set(cur.bsSamples);

    selectedX.setVisibility(true);
    selectedY.setVisibility(true);
    deleteP.setVisibility(true);
    drawControl.setVisibility(true);
    closeCurve.setVisibility(true);
    clearCurve.setVisibility(true);

    // pull all information down from the canvas and set panel items
    switch(cur.getType().value){
      
      // curve type is bezier DCJ
      case 0:
        iterations.setVisibility(true);
        bezierDCJ.set(1.0);
        break;
        
      // curve type is bezier SUB
      case 1:
        subdivisions.setVisibility(true);
        bezierSub.set(1.0);
        break;
        
      // curve type is b-spline
      case 2:
        bsSamples.setVisibility(true);
        bspline.set(1.0);
        order.setVisibility(true);
        break;
    }
  }
}

void updateSurface(){
  if(surfaceDirty){
    surfaceDirty = false;
    
    surfaceViewport.showSub = false;
    surfaceViewport.hasSubMesh = false;
    original.set(1.0);
    triangulate.set(0.0);
    
    // if extrusion, pull xyCurve and extrude
    if(extrusion.value == 1.0){
      extrude(xyCanvas.getCurve(), surfaceViewport, int(sliceNum.value), sliceSize.value);
    }
    // if rotation, pull xyCurve and rotate
    else if(rotation.value == 1.0){
      rotate(xyCanvas.getCurve(), surfaceViewport, int(sliceNum.value), angleRot.value);
    }
    
    // if sweep, pull both curves and sweep
    else if(sweep.value == 1.0){
      sweep(xyCanvas.getCurve(), yzCanvas.getCurve(), surfaceViewport);
    }
  }
}

void subdivideSurface(){
  Mesh subM = null;

  original.set(1.0);
  surfaceViewport.hasSubMesh = false;
  surfaceViewport.showSub = false;
  surfaceViewport.subMesh = null;

  if(bezierSubdivision.value == 1.0){
    subM = bezierSubdivision(surfaceViewport.mesh, int(subWidth.value), int(subHeight.value));
  }
  
  else if(bsplineSubdivision.value == 1.0){
    subM = bsplineSubdivision(surfaceViewport.mesh, int(subWidth.value), int(subHeight.value));
  }
  
  else if(dooSabinSubdivision.value == 1.0){
    subM = dooSabin(surfaceViewport.mesh, int(subIterations.value));
  }
  
  else if(loopSubdivision.value == 1.0){
    subM = loopSubdivision(surfaceViewport.mesh, int(subIterations.value));
  }
  
  if(subM != null){
    original.set(0.0);
    surfaceViewport.hasSubMesh = true;
    surfaceViewport.showSub = true;
    surfaceViewport.subMesh = subM;
  }
}

void setSurfaceOptions(PanelItem item){
  if(item.value == 0.0){
    surfaceDirty = true;
    extrusion.set(0.0);
    rotation.set(0.0);
    sweep.set(0.0);
    item.set(1.0);
    
    sliceNum.setVisibility(false);
    sliceSize.setVisibility(false);
    angleRot.setVisibility(false);
    
    if(item == extrusion){
      sliceNum.setVisibility(true);
      sliceSize.setVisibility(true);
    }
    else if(item == rotation){
      sliceNum.setVisibility(true);
      angleRot.setVisibility(true);
    }
  }
}

void setSubdivision(PanelItem item){
  if(item.value == 0.0){
    bezierSubdivision.set(0.0);
    bsplineSubdivision.set(0.0);
    dooSabinSubdivision.set(0.0);
    loopSubdivision.set(0.0);
    item.set(1.0);

    subWidth.setVisibility(false);
    subHeight.setVisibility(false);
    subIterations.setVisibility(false);
   
    if(item == bezierSubdivision || item == bsplineSubdivision){
      subWidth.setVisibility(true);
      subHeight.setVisibility(true);
    }
    
    else if(item == dooSabinSubdivision || item == loopSubdivision){
      subIterations.setVisibility(true);
    }
  }
}

void mousePressed(){
  PanelItem item = rightPanel.getEvent();

  // if no event for right panel, check top panel
  if(item == null) item = topPanel.getEvent();

  // we have an event from either panel
  if(item != null){

    // if we are switching curve type
    if(item == bezierSub || item == bezierDCJ || item == bspline){
      setCurveOptions(item);
    }

    else if(item == extrusion || item == rotation || item == sweep){
      setSurfaceOptions(item);
    }
    
    else if(item == bezierSubdivision || item == bsplineSubdivision ||
            item == dooSabinSubdivision || item == loopSubdivision){
      setSubdivision(item);
    }
    
    else if(item == surfaceItem){
      loadSurfaceOptions(item);
    }
    
    // switch between viewports
    else if(item == curveXY || item == curveYZ){
      loadCurveOptions(item);
    }
    
    else if(item == drawControl){
      item.handlePush();
      xyCanvas.drawControlPolygon(item.get() == 1.0);
      yzCanvas.drawControlPolygon(item.get() == 1.0);
    }
    
    else if(item == wireframe){
      item.handlePush();
      surfaceViewport.setWireframe(item.get() == 1.0);
    }
    
    else if((item == clearCurve) || (item == deleteP) ||
            (item == subdivide) || (item == triangulate)){
      
      // if there is a change to the xyCanvas or we are using both curves
      // surface is dirty
      if(activeViewport == xyCanvas || sweep.value == 1.0){
        surfaceDirty = true;
      }
      
      item.handlePush();
      mouseDown = true;
      downItem = item;
    }
    
    else if(item == closeCurve){
      item.handlePush();
      if(activeViewport == xyCanvas || activeViewport == yzCanvas){
        ((CurveCanvas)activeViewport).setClosed(item.get() == 1.0);
      }
      // if there is a change to the xyCanvas or we are using both curves
      // surface is dirty
      if(activeViewport == xyCanvas || sweep.value == 1.0){
        surfaceDirty = true;
      }
    }
    
    else if(item == meshOut || item == meshIn){
      mouseDown = true;
      downItem = item;
      item.handlePush();
    }
    
    else if(item == original){
      if(surfaceViewport.hasSubMesh){
        item.handlePush();
        surfaceViewport.showSub = (original.value == 1.0 ? false : true);
      }
    }
    
    else{
      item.handlePush();
    }
  }
  
  // interacting with a canvas
  if(activeViewport.event()){
    activeViewport.handlePush();
    
    if(activeViewport == xyCanvas || activeViewport == yzCanvas){
      Point p = ((CurveCanvas)activeViewport).getSelected();
      selectedX.set(p.x);
      selectedY.set(p.y);
      order.range(new Point(3, max(((CurveCanvas)activeViewport).size(), 3)));
    }
  }
}

void mouseDragged(){
  PanelItem item = rightPanel.getEvent();

  // if no event for right panel, check top panel
  if(item == null){
    item = topPanel.getEvent();
  }
  
  if(item != null){
    item.handleDrag();
    
    if(item == iterations){
      ((CurveCanvas)activeViewport).setIterations(int(iterations.get()));
    }
    else if(item == subdivisions){
      ((CurveCanvas)activeViewport).setSubdivisions(int(subdivisions.get()));
    }

    else if(item == bsSamples){
      ((CurveCanvas)activeViewport).setBsSamples(int(bsSamples.get()));
    }
    
    else if(item == order){
      ((CurveCanvas)activeViewport).setOrder(int(order.get()));
    }
    else if(item == sliceNum || item == sliceSize || item == angleRot){
      surfaceDirty = true;
    }
  }
  
  else if(activeViewport.event()){
    activeViewport.handleDrag();

    if(activeViewport == xyCanvas || activeViewport == yzCanvas){
      Point p = ((CurveCanvas)activeViewport).getSelected();
      selectedX.set(p.x);
      selectedY.set(p.y);
    }
  }
}

void mouseReleased(){
  
  if(mouseDown){
    
    downItem.handlePush();
    
    if(downItem == clearCurve){
      ((CurveCanvas)activeViewport).clear();
      order.range(new Point(3, 3));
      selectedX.set(0.0);
      selectedY.set(0.0);
      order.set(3.0);
    }
    
    else if(downItem == deleteP){
      ((CurveCanvas)activeViewport).deleteVertex();
      order.range(new Point(3, max(((CurveCanvas)activeViewport).size(), 3)));
      order.set(max(min(order.get(), ((CurveCanvas)activeViewport).size()), 3));
      Point p = ((CurveCanvas)activeViewport).getSelected();
      selectedX.set(p.x);
      selectedY.set(p.y);
    }
    
    else if(downItem == meshOut){
      objData = surfaceViewport.getObj();
      if(objData.length > 0){
        fileMode = true;
      }
    }
    
    else if(downItem == meshIn){
      fileMode = true;
      inputFileMode = true;
    }
    
    else if(downItem == subdivide){
      subdivideSurface();
//      triangulate.set(0.0);
    }

    else if(downItem == triangulate){
      if(surfaceViewport.showSub){
        triangulate(surfaceViewport.subMesh);
      }
      else{
        triangulate(surfaceViewport.mesh);
      }
    }

    mouseDown = false;
    downItem = null;
    return;
  }
  
  PanelItem item = rightPanel.getEvent();

  // if no event for right panel, check top panel
  if(item == null) item = topPanel.getEvent();
  
  if(item != null){
    item.handleRelease();
  }
  else if(activeViewport.event()){
    activeViewport.handleRelease();
  }
}

void keyPressed(){
  
  // get keys
  if(fileMode){
    
    if(key == ENTER || key == RETURN){
      fileMode = false;
      
      if(inputFileMode){
        inputFileMode = false;
        
        Mesh mesh = loadObj(outputFile);
        if(mesh != null){
          surfaceViewport.mesh = mesh;
          original.set(1.0);
          surfaceViewport.hasSubMesh = false;
          surfaceViewport.showSub = false;
          surfaceViewport.subMesh = null;
          
          extrusion.set(0.0);
          rotation.set(0.0);
          sweep.set(0.0);
          sliceNum.setVisibility(false);
          sliceSize.setVisibility(false);
          angleRot.setVisibility(false);
        }
      }
      
      else{
        saveStrings(outputFile, objData);
      }
    }
    else if((key == BACKSPACE || key == DELETE)){
      if(outputFile.length() > 0){
        outputFile = outputFile.substring(0, outputFile.length() - 1);
      }
      else{
        outputFile = "";
      }
    }
    else if(key == ESC){
      fileMode = false;
      key = 0;
    }
    else{
      outputFile += key;
    }
  }
}
