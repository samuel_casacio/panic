// Samuel Casacio
// CPSC 881 Geometric Modeling
// CurveType
// dealing with java enums :(

// enum for implemented curve types
enum CurveType {
  BEZDCJ (0), BEZSUB(1), BS (2);
  int value;
  
  CurveType(int value){
    this.value = value;
  }
  
  boolean equals(CurveType rhs){
    return value == rhs.value;
  }

};
