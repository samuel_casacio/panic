// Samuel Casacio
// CPSC 881 Geometric Modeling
// PointData
// simple class to track point data

class PointData{

  ArrayList<Point> data;
  boolean closed;
  
  PointData(){
    data = new ArrayList<Point>();
    closed = false;
  }
  
  void clear(){
    data.clear();
  }
  
  void remove(int index){
    data.remove(index);
  }
  
  void setClosed(boolean v){
    closed = v;
  }
  
  Point get(int index){
    return data.get(index);
  }
  
  Point first(){
    return data.get(0);
  }
  
  Point last(){
    return data.get(data.size() - 1);
  }
  
  int size(){
    return data.size();
  }
  
  void add(Point p){
    data.add(p);
  }
  
  void add(Point p, int index){
    data.add(index, p);
  }
  
  // return the index of the closest point within tol range
  // -1 if nothing in range
  int get(float x, float y, float tol){
    Point p;
    int index = -1;
    float d = MAX_FLOAT;
    float tempd;
    
    for(int i = 0; i < data.size(); i++){
      p = data.get(i);
      tempd = sqrt( (p.x - x)*(p.x - x) + (p.y - y)*(p.y - y) );
      if((tempd < d) && (tempd <= tol)){
        d = tempd;
        index = i;
      }
    }
    
    return index;
  }
  
  // because java doesnt do deep copy well...
  PointData deepCopy(){
    PointData copy = new PointData();
    for(int i = 0; i < data.size(); i++)
      copy.add(new Point(data.get(i)));
    
    return copy;
  }
}

// concat 2 arrays with another
PointData pdConcat(PointData lhs, PointData rhs){
  PointData ret = new PointData();
  for(int i = 0; i < lhs.size(); i++) ret.add(new Point(lhs.get(i)));
  for(int i = 0; i < rhs.size(); i++) ret.add(new Point(rhs.get(i)));
  return ret;
}
