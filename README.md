# panic
Author: Samuel Casacio

Tested with:
Processing Version: 2.1.1

How To Execute:
Open panic in Processing, hit the play button.

Usage:
The UI is pretty straight forward.  To build a curve, just click the mouse in the grey plot area to start adding control polygon verticies.  Verticies will turn green when the mouse hovers over them, clicking on a vertex will select it.  By default, the last vertex places is the selected vertex.  The first vertex is yellow, the last vertex is blue, the vertex the mouse is hovering over is green, and the selected vertex is red.  Clicking the plot area will insert a new vertex after the selected vertex.  You can also drag and drop verticies.

You can switch between 2 curves and a surface.  In surface mode you can rotate the camera by left clicking and dragging the mouse.  Right clicking and dragging the mouse will zoom in and out.  You can output a obj file by clicking the output obj button and entering a file name (there is a default one there, use backspace or delete to clear it out).  Pressing enter will save the mesh, pressing esc will cancel.  Loading a mesh is performed similarly.

You can select a subdivision algorithm to use on a mesh.  Pick your sudivision, select your options, and click the subdivide button.  If the subdivision cannot be performed on the input mesh, nothing will happen.  You can switch between the original mesh and the subdivided mesh by clicking on the "original" button.  Subdivision will only be performed on the original mesh, you cannot do one subdivision then use that as an input for another.

NOTE: I had some trouble getting processing to properly display high valence triangles.  When I export the obj and load it into blender it shows up fine.  This only happens when doing Doo-Sabin subdivision.

NOTE2: B-spline subdivision uses non-uniform knot vectors.

Features Implemented:
Triangulation (just to a fan from the first vertex in the face)
Obj loading
Bezier Subdivision (cannot be done on loaded obj)
B-Spline Subdivision (this is using non-uniform knot vectors, cannot be done on loaded obj)
Doo-Sabin Subdivision
Loop Subdivision (mesh must be a triangle mesh, use triangulate to verify)
Extrusion
Rotation (about the x axis)
Sweep
Wireframe mode
OBJ output
complete GUI rework
Bezier (de Casteljau)
Bezier (subdivision)
Arbitrary B-Spline (de Boor, knots are non-uniform and procedurally generated)
Control polygon manipulation through dragging and dropping and clicking
Add extra verticies in any order to control polygon (ie, can place a new vertex between 2 verticies in the control polygon)
Close Curve option
Draw Control Polygon option
Ability to increase/decrease de Casteljau iterations and subdivions for Bezier
Only regenerates curve if there is a change in the curve
Display point data
Specific vertex deletion
Clear curve data
Set number of B-Spline samples
Increase/Decrease B-Spline order (caps at # of verticies in control polygon)
