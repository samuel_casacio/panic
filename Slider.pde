// Samuel Casacio
// CPSC 881 Geometric Modeling
// Slider
// An item for the side panel

class Slider extends PanelItem{

  Point range;
  Point line;
  float pad;
  boolean active;
  
  Slider(String label, Point LLC, Point URC, boolean show, boolean horizontal){
    super(label, LLC, URC, show, horizontal);
    pad = (URC.x - LLC.x) / 12;
    float length = (URC.x - LLC.x) * (2.0/3.0);
    line = new Point();
    range = new Point();
    line.x = LLC.x + pad;
    line.y = LLC.x + length + pad;
    active = false;
  }
  
  void setOffset(int offset){
    if(horizontal){
      localLC.x = LLC.x - float(offset) * 150.0;
      localRC.x = URC.x - float(offset) * 150.0;
    }
    else{
      localLC.y = LLC.y - float(offset) * 40.0;
      localRC.y = URC.y - float(offset) * 40.0;      
    }
    line.x = localLC.x + pad;
    line.y = localLC.x + (URC.x - LLC.x) * (2.0/3.0) + pad;
  }
  
  void draw(){
    
    if(!show) return;
    
    // grey out the button if pushed
    fill(255);
    stroke(0);
    rect(localLC.x, localRC.y, localRC.x - localLC.x, localLC.y - localRC.y);

    fill(0);
    stroke(0);
    strokeWeight(2);
    line(line.x, localLC.y + 15, line.y, localLC.y + 15);
    
    fill(0);
    textSize(12);
    text(str(int(range.x)), line.x - textWidth(str(int(range.x))) * 0.5, localLC.y + 30);
    text(str(int(range.y)), line.y - textWidth(str(int(range.y))) * 0.5, localLC.y + 30);
    text(label, line.x + (line.y - line.x - textWidth(label)) * 0.5, localLC.y + 37);

    textSize(16);
    float x = line.y + (textWidth(str(int(value))) * 0.5) + pad * 0.5;
    text(str(int(value)), x, localLC.y + 20);
    
    fill(100);
    strokeWeight(1);
    x = map(value, range.x, range.y, line.x, line.y);
    rect(x - 3, localLC.y + 5, 6, 20);
  }

  void handlePush(){
    int x = int(map(value, range.x, range.y, line.x, line.y));
    if(mouseX >= x - 4 && mouseX <= x + 4 && mouseY >= localLC.y + 4 && mouseY <= localLC.y + 26){
      active = true;
    }
  }
  
  void handleDrag(){
    if(active){
      value = map(min(max(float(mouseX), line.x), line.y), line.x, line.y, range.x, range.y); 
    }
  }
  
  void handleRelease(){
    active = false;
  }
  
  boolean event(){
    if((mouseX >= int(localLC.x) && mouseX <= int(localRC.x) && mouseY >= int(localLC.y) && mouseY <= int(localRC.y) && show) || active){
      return true;
    }
    return false;
  }
  
  void range(Point p){
    range = p;
  }
  
  float get(){
    return min(range.y, max(value, range.x));
  }
}
