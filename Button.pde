// Samuel Casacio
// CPSC 881 Geometric Modeling
// Button
// An item for the side panel

class Button extends PanelItem{
  
  Button(String label, Point LLC, Point URC, boolean show, boolean horizontal){
    super(label, LLC, URC, show, horizontal);
  }
  
  void draw(){
    
    if(!show) return;
    
    // grey out the button if not on
    fill(200);
    if(value == 1.0) fill(150, 200, 150);
    rect(localLC.x, localRC.y, localRC.x - localLC.x, localLC.y - localRC.y);

    // outline
    noFill();
    stroke(0);
    rect(localLC.x, localRC.y, localRC.x - localLC.x, localLC.y - localRC.y);

    textSize(20);
    fill(0);
    stroke(0);

    int texts = 20;
    textSize(texts);
    while(textWidth(label) > localRC.x - localLC.x){
      texts--;
      textSize(texts);
    }

    float x = ((localRC.x - localLC.x) - textWidth(label)) * 0.5;
    text(label, localLC.x + x, localRC.y - 10);
  }
  
  void handlePush(){
    value = value == 1.0 ? 0.0 : 1.0;
  }
  
  boolean event(){
    if(mouseX >= int(localLC.x) && mouseX <= int(localRC.x) && mouseY >= int(localLC.y) && mouseY <= int(localRC.y) && show){
      return true;
    }
    return false;
  }
}
