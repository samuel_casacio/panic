// Samuel Casacio
// CPSC 881 Geometric Modeling
// Surfacing
// some quick surfacing functions

void extrude(PointData xyCurve, SurfaceView view, int slices, float ds){
  Mesh mesh = view.mesh;
  mesh.clear();
  mesh.wClosed = xyCurve.closed;
  mesh.hClosed = false;
  int span = xyCurve.size();
  Point p;

  // dont add final vertex if curve is closed
  if(xyCurve.closed){
    span--;
  }
  
  mesh.isPatch = true;
  mesh.width = span;
  mesh.height = slices;
  
  // add all the verts to the mesh
  for(int i = 0; i < slices; i++){
    float offset = i * ds;
    for(int j = 0; j < span; j++){
      
      p = xyCurve.get(j);
      float x = -500.0 + 1000.0 * ((p.x - view.LLC.x) / (view.URC.x - view.LLC.x));
      float y = ((view.URC.y - view.LLC.y) - (p.y - view.LLC.y));
      Vertex v = new Vertex(x, y, -500.0 + offset);
      mesh.addVertex(v);
    }
  }
  
  // configure faces
  int v0;
  int v1;
  int v2;
  int v3;
  for(int i = 0; i < slices - 1; i++){
    for(int j = 0; j < span - 1; j++){
      v0 = ( (j + 0) + (span * (i + 0)) );
      v1 = ( (j + 1) + (span * (i + 0)) );
      v2 = ( (j + 0) + (span * (i + 1)) );
      v3 = ( (j + 1) + (span * (i + 1)) );
      
      Tuple f = new Tuple();
      f.add(v0);
      f.add(v2);
      f.add(v3);
      f.add(v1);
      mesh.addFace(f);
    }
    
    if(xyCurve.closed){
      v0 = ( (span - 1) + (span * (i + 0)) );
      v1 = ( 0          + (span * (i + 0)) );
      v2 = ( (span - 1) + (span * (i + 1)) );
      v3 = ( 0          + (span * (i + 1)) );
      
      Tuple f = new Tuple();
      f.add(v0);
      f.add(v2);
      f.add(v3);
      f.add(v1);
      mesh.addFace(f);
    }
  }
}

void rotate(PointData xyCurve, SurfaceView view, int slices, float angle){
	Mesh mesh = view.mesh;
  mesh.clear();
  int span = xyCurve.size();
  mesh.wClosed = xyCurve.closed;
  mesh.hClosed = false;
  Point p;
  float dt = (angle / 180 * PI) / float(slices - 1);
  
  // close the surface if we are doing a full rotation
  if(angle == 360){
    slices--;
    mesh.hClosed = true;
  }

  // dont add final vertex if curve is closed
  if(xyCurve.closed){
    span--;
  }

  mesh.isPatch = true;
  mesh.width = span;
  mesh.height = slices;
  
  // add all the verts to the mesh
  for(int i = 0; i < slices; i++){

    float theta = float(i) * dt;
    for(int j = 0; j < span; j++){
      p = xyCurve.get(j);
      float x = -500.0 + 1000.0 * ((p.x - view.LLC.x) / (view.URC.x - view.LLC.x));
      float y = ((view.URC.y - view.LLC.y) - (p.y - view.LLC.y));
      
      // rotation around x-axis
      Vertex v = new Vertex(x, y * cos(theta), y * sin(theta));
      mesh.addVertex(v);
    }
  }
  
  // configure faces
  int v0;
  int v1;
  int v2;
  int v3;
  for(int i = 0; i < slices - 1; i++){
    for(int j = 0; j < span - 1; j++){
      v0 = ( (j + 0) + (span * (i + 0)) );
      v1 = ( (j + 1) + (span * (i + 0)) );
      v2 = ( (j + 0) + (span * (i + 1)) );
      v3 = ( (j + 1) + (span * (i + 1)) );

      Tuple f = new Tuple();
      f.add(v0);
      f.add(v2);
      f.add(v3);
      f.add(v1);
      mesh.addFace(f);
    }

    if(xyCurve.closed){
      v0 = ( (span - 1) + (span * (i + 0)) );
      v1 = ( 0          + (span * (i + 0)) );
      v2 = ( (span - 1) + (span * (i + 1)) );
      v3 = ( 0          + (span * (i + 1)) );
      
      Tuple f = new Tuple();
      f.add(v0);
      f.add(v2);
      f.add(v3);
      f.add(v1);
      mesh.addFace(f);
    }
  }
  
  // close the surface!
  if(angle == 360){
    for(int j = 0; j < span - 1; j++){
      v0 = ( (j + 0) + (span * (slices - 1)) );
      v1 = ( (j + 1) + (span * (slices - 1)) );
      v2 = ( (j + 0) );
      v3 = ( (j + 1) );

      Tuple f = new Tuple();
      f.add(v0);
      f.add(v2);
      f.add(v3);
      f.add(v1);
      mesh.addFace(f);
    }
    
    if(xyCurve.closed){
      v0 = ( (span - 1) + (span * (slices - 1)) );
      v1 = ( 0          + (span * (slices - 1)) );
      v2 = ( (span - 1) );
      v3 = ( (0)        );
      
      Tuple f = new Tuple();
      f.add(v0);
      f.add(v2);
      f.add(v3);
      f.add(v1);
      mesh.addFace(f);
    }
  }
}

void buildFaces(Mesh mesh){

  // configure faces
  int v0;
  int v1;
  int v2;
  int v3;
  for(int i = 0; i < mesh.height - 1; i++){
    for(int j = 0; j < mesh.width - 1; j++){
      
      v0 = ( (j + 0) + (mesh.width * (i + 0)) );
      v1 = ( (j + 1) + (mesh.width * (i + 0)) );
      v2 = ( (j + 0) + (mesh.width * (i + 1)) );
      v3 = ( (j + 1) + (mesh.width * (i + 1)) );

      Tuple f = new Tuple();
      f.add(v0);
      f.add(v2);
      f.add(v3);
      f.add(v1);
      mesh.addFace(f);
    }

    if(mesh.wClosed){
      v0 = ( (mesh.width - 1) + (mesh.width * (i + 0)) );
      v1 = ( 0                   + (mesh.width * (i + 0)) );
      v2 = ( (mesh.width - 1) + (mesh.width * (i + 1)) );
      v3 = ( 0                   + (mesh.width * (i + 1)) );
      
      Tuple f = new Tuple();
      f.add(v0);
      f.add(v2);
      f.add(v3);
      f.add(v1);
      mesh.addFace(f);
    }
  }

  if(mesh.hClosed){
    for(int j = 0; j < mesh.width - 1; j++){
      v0 = ( (j + 0) + (mesh.width * (mesh.height - 1)) );
      v1 = ( (j + 1) + (mesh.width * (mesh.height - 1)) );
      v2 = ( (j + 0) );
      v3 = ( (j + 1) );
      
      Tuple f = new Tuple();
      f.add(v0);
      f.add(v2);
      f.add(v3);
      f.add(v1);
      mesh.addFace(f);
    }
    
    if(mesh.wClosed){
      v0 = ( (mesh.width - 1) + (mesh.width * (mesh.height - 1)) );
      v1 = ( 0                   + (mesh.width * (mesh.height - 1)) );
      v2 = ( (mesh.width - 1) );
      v3 = 0;
      
      Tuple f = new Tuple();
      f.add(v0);
      f.add(v2);
      f.add(v3);
      f.add(v1);
      mesh.addFace(f);
    }
  }
}

void sweep(PointData xyCurve, PointData yzCurve, SurfaceView view){
	Mesh mesh = view.mesh;
  mesh.clear();
  mesh.wClosed = xyCurve.closed;
  mesh.hClosed = yzCurve.closed;
  Point xy;
  Point yz;
  
  int yzspan = yzCurve.size();
  int xyspan = xyCurve.size();
  
  if(xyCurve.closed){
    xyspan--;
  }
  if(yzCurve.closed){
    yzspan--;
  }
  
  mesh.isPatch = true;
  mesh.width = xyspan;
  mesh.height = yzspan;
  
  for(int i = 0; i < yzspan; i++){
    
    yz = yzCurve.get(i);
    float yyz = ((view.URC.y - view.LLC.y) - (yz.y - view.LLC.y));
    float z = -500.0 + 1000.0 * ((yz.x - view.LLC.x) / (view.URC.x - view.LLC.x));
    for(int j = 0; j < xyspan; j++){
      xy = xyCurve.get(j);
      
      float x = -500.0 + 1000.0 * ((xy.x - view.LLC.x) / (view.URC.x - view.LLC.x));
      float yxy = ((view.URC.y - view.LLC.y) - (xy.y - view.LLC.y));
      
      Vertex v = new Vertex(x, yxy + yyz, z);
      mesh.addVertex(v);
    }
  }
  
  buildFaces(mesh);
}

// linear interpolation for vertices
void LERP(Vertex v1, Vertex v2, Vertex out, float t){
  out.x = (1.0 - t) * v1.x + t * v2.x;
  out.y = (1.0 - t) * v1.y + t * v2.y;
  out.z = (1.0 - t) * v1.z + t * v2.z;
}

// reduce the array to an interpolated vertex on the surface
void dcReduce(ArrayList<Vertex> data, float t){
  Vertex v1, v2;
  
  // reduce the size of the array until the last item is a point on the curve
  while(data.size() > 1){
    
    // reduce the size of the data through interpolation...
    for(int i = 1; i < data.size(); i++){
      v1 = data.get(i-1);
      v2 = data.get(i);
      LERP(v1, v2, v1, t);
    }
    data.remove(data.size()-1);
  }
}

// deep copy of row
ArrayList<Vertex> copyRow(ArrayList<Vertex> data, int start, int width){
  ArrayList<Vertex> ret = new ArrayList<Vertex>(width);
  
  for(int i = 0; i < width; i++){
    ret.add(data.get(start + i).deepCopy());
  }
  
  return ret;
}

Mesh bezierSubdivision(Mesh surface, int wDivisions, int hDivisions){

  // cant be done if not a bezier patch
  if(!surface.isPatch || surface.width <= 2 || surface.height < 2){
    return null;
  }

  Mesh subMesh = new Mesh();
  float dw = 1.0 / float(surface.width * wDivisions - 1);
  float dh = 1.0 / float(surface.height * hDivisions - 1);
  
  subMesh.width = surface.width * wDivisions;
  subMesh.height = surface.height * hDivisions;

  for(int h = 0; h < surface.height * hDivisions; h++){
    for(int w = 0; w < surface.width * wDivisions; w++){
      
      ArrayList<Vertex> q = new ArrayList<Vertex>(surface.height);
      
      // first iteration of dc
      for(int i = 0; i < surface.height; i++){
        ArrayList<Vertex> row = copyRow(surface.verts, i * surface.width, surface.width);
        dcReduce(row, float(w) * dw);
        q.add(row.get(0));
      }
      
      // second iteration
      dcReduce(q, float(h) * dh);
      subMesh.addVertex(q.get(0));
    }
  }
  
  subMesh.isPatch = true;
  subMesh.wClosed = surface.wClosed;
  subMesh.hClosed = surface.hClosed;

  buildFaces(subMesh);

  return subMesh;
}

ArrayList<Vertex> bSpline(ArrayList<Vertex> verts, int D, int samples){
  
  ArrayList<Vertex> ret = new ArrayList<Vertex>(samples);
  
  // create the list of knots
  int n = verts.size() - 1;
  int T = n + D + 1;  // degree
  ArrayList<Float> knots = new ArrayList<Float>(T);
  for(int i = 0; i < T; i++){
//    knots.add(new Float(i));
    if(i < D) knots.add(new Float(0));
    else if(D <= i && i <= n) knots.add(new Float(i - D + 1));
    else if(n < i && i <= n + D) knots.add(new Float(n - D + 2));
  }
  
  // sample a bunch of times over the spline
//  float dt = float(T - 1) / float(samples);
  float dt = float(n - D + 2) / float(samples - 1);
  for(int j = 0; j < samples; j++){

    Vertex newp = new Vertex();
//    float t = min(float(j) * dt, float(T - 1) - 0.001);
    float t = min(float(j) * dt, float(n - D + 2) - 0.001);

    for(int i = 0; i <= n; i++){
      float alpha = deBoor(knots, i, D, t);
      Vertex pi = verts.get(i);
      newp.x += pi.x * alpha;
      newp.y += pi.y * alpha;
      newp.z += pi.z * alpha;
    }
    
    ret.add(newp);
  }
  
  return ret;
}

Mesh bsplineSubdivision(Mesh surface, int wDivisions, int hDivisions){
  
  // need to have a surface that can be subdivided AND
  // has the correct number of vertices
  if(!surface.isPatch || surface.width <= 2 || surface.height <= 2){
    return null;
  }

  Mesh subMesh = new Mesh();
  
  subMesh.width = surface.width * wDivisions;
  subMesh.height = surface.height * hDivisions;

  // first gather up each subdivided row
  ArrayList<ArrayList<Vertex> > columns = new ArrayList<ArrayList<Vertex> >(surface.width);
  
  // inner sum
  for(int w = 0; w < surface.width; w++){
    
    ArrayList<Vertex> column = new ArrayList<Vertex>(surface.height);
    
    // copy the column
    for(int h = 0; h < surface.height; h++){
      column.add(surface.verts.get(surface.width * h + w));
    }
    
    // run bspline algorithm on column to get row
    column = bSpline(column, 3, subMesh.height);
    columns.add(column);
  }
  
  // outer sum
  for(int j = 0; j < subMesh.height; j++){
    
    // copy the row
    ArrayList<Vertex> row = new ArrayList<Vertex>(surface.width);
    
    for(int i = 0; i < columns.size(); i++){
      ArrayList<Vertex> column = columns.get(i);
      row.add(column.get(j));
    }
    
    row = bSpline(row, 3, subMesh.width);

    for(int i = 0; i < row.size(); i++){
      Vertex v = row.get(i);
      subMesh.addVertex(v);
    }
  }

  subMesh.isPatch = true;
  subMesh.wClosed = surface.wClosed;
  subMesh.hClosed = surface.hClosed;

  buildFaces(subMesh);

  return subMesh;
}
