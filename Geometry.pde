// Samuel Casacio
// CPSC 881 Geometric Modeling
// Vertex, Edge, Triangle, Quad
// Definition for some basic geometric types

class Vertex{
  float x;
  float y;
  float z;
  
  Vertex(){
    x = 0.0;
    y = 0.0;
    z = 0.0;
  }
  
  Vertex(float x, float y, float z){
    this.x = x;
    this.y = y;
    this.z = z;
  }
  
  Vertex(Vertex v){
    x = v.x;
    y = v.y;
    z = v.z;
  }
  
  Vertex deepCopy(){
    return new Vertex(x, y, z);
  }
  
  Vertex add(Vertex rhs){
    return new Vertex(x + rhs.x, y + rhs.y, z + rhs.z);
  }
  
  Vertex sub(Vertex rhs){
    return new Vertex(x - rhs.x, y - rhs.y, z - rhs.z);
  }
  
  void assign(Vertex rhs){
    x = rhs.x;
    y = rhs.y;
    z = rhs.z;
  }
  
  boolean equals(Vertex rhs){
    if(x == rhs.x && y == rhs.y && z == rhs.z){
      return true;
    }
    
    return false;
  }
  
  float length(){
    return sqrt(x * x + y * y + z * z);
  }
  
  String toString(){
    String ret = "[" + str(x) + ", " + str(y) + ", " + str(z) + "]";
    return ret;
  }
}

float dot(Vertex v1, Vertex v2){
  return (v1.x * v2.x + v1.y * v2.y + v1.z * v2.z);
}

class Edge{
  Vertex v0;
  Vertex v1;
  
  Edge(){
    v0 = new Vertex();
    v1 = new Vertex();
  }
  
  Edge(Vertex v0, Vertex v1){
    this.v0 = v0;
    this.v1 = v1;
  }
  
  Edge(Edge e){
    this.v0 = e.v0;
    this.v1 = e.v1;
  }

  Edge deepCopy(){
    Edge e = new Edge();
    e.v0.assign(v0);
    e.v1.assign(v1);
    return e;
  }
  
  void assign(Vertex v0, Vertex v1){
    this.v0 = v0;
    this.v1 = v1;
  }
}

class Triangle{
  Vertex v0;
  Vertex v1;
  Vertex v2;
  
  Triangle(){
    v0 = new Vertex();
    v1 = new Vertex();
    v2 = new Vertex();
  }

  Triangle(Vertex v0, Vertex v1, Vertex v2){
    this.v0 = v0;
    this.v1 = v1;
    this.v2 = v2;
  }
  
  Triangle(Triangle t){
    v0 = t.v0;
    v1 = t.v1;
    v2 = t.v2;
  }

  Triangle deepCopy(){
    Triangle t = new Triangle();
    t.v0.assign(v0);
    t.v1.assign(v1);
    t.v2.assign(v2);
    return t;
  }
  
  void assign(Vertex v0, Vertex v1, Vertex v2){
    this.v0 = v0;
    this.v1 = v1;
    this.v2 = v2;
  }
}

class Quad{
  Vertex v0;
  Vertex v1;
  Vertex v2;
  Vertex v3;
  
  Quad(){
    v0 = new Vertex();
    v1 = new Vertex();
    v2 = new Vertex();
    v3 = new Vertex();
  }
  
  Quad(Vertex v0, Vertex v1, Vertex v2, Vertex v3){
    this.v0 = v0;
    this.v1 = v1;
    this.v2 = v2;
    this.v3 = v3;
  }
  
  Quad(Quad q){
    v0 = q.v0;
    v1 = q.v1;
    v2 = q.v2;
    v3 = q.v3;
  }
  
  Quad deepCopy(){
    Quad q = new Quad();
    q.v0.assign(v0);
    q.v1.assign(v1);
    q.v2.assign(v2);
    q.v3.assign(v3);
    return q;
  }
  
  void convertToTriangles(Triangle t1, Triangle t2){
    t1.assign(v0, v1, v2);
    t2.assign(v3, v2, v1);
  }
  
  void assign(Vertex v0, Vertex v1, Vertex v2, Vertex v3){
    this.v0 = v0;
    this.v1 = v1;
    this.v2 = v2;
    this.v3 = v3;
  }
}
