// Samuel Casacio
// CPSC 881 Geometric Modeling
// SurfaceView
// Display a surface

class SurfaceView extends Viewport{
  
  // mesh data
  Mesh mesh;
  Mesh subMesh;
  
  boolean grid;
  boolean wireframe;
  boolean showSub;
  boolean hasSubMesh;

  // render data
  PGraphics viewport;
  
  // camera position (static view position)
  float r;
  Vertex cPos;
  Point prev;
  Point current;
  
  SurfaceView(Point llc, Point urc){
    super(llc, urc);
    
    float w = URC.x - LLC.x;
    float h = URC.y - LLC.y;
    
    viewport = createGraphics(int(w), int(h), P3D);
   
    mesh = new Mesh();
    grid = true;
    
    cPos = new Vertex();
    
    float theta = 0.0;
    float phi = PI * 0.25;
    r = 1000.0;
    
    cPos.x = r * sin(phi) * cos(theta);
    cPos.y = r * cos(phi);
    cPos.z = r * sin(phi) * sin(theta);
   
    prev = new Point();
    current = new Point();
    
    wireframe = false;
    showSub = false;
    hasSubMesh = false;
  }
  
  void setWireframe(boolean wireframe){
    this.wireframe = wireframe;
  }
  
  void drawGrid(){

    viewport.noFill();
    viewport.stroke(225);
    viewport.strokeWeight(1);
    
    for(int x = -500; x <= 500; x += 50){
      viewport.stroke(200);
      viewport.line(x, 0, -500, x, 0, 500);
    }

    for(int z = -500; z <= 500; z += 50){
      viewport.stroke(200);
      viewport.line(-500, 0, z, 500, 0, z);
    }
  }

  String[] getObj(){
    
    ArrayList<String> obj = new ArrayList<String>();
    
    Mesh dMesh = mesh;
    if(showSub){
      dMesh = subMesh;
    }
    
    if(dMesh.numFaces() > 0){

      obj.add("# Mesh Created By PANIC");
      
      // output all the verticies
      Vertex v;
      String line = new String();
      for(int i = 0; i < dMesh.numVerts(); i++){
        v = dMesh.getVertex(i);
          line = ("v " + str(v.x) + " " + str(v.y) + " " + str(v.z));
          obj.add(line);
      }
      
      obj.add("");
      
      // output all faces
      Tuple f;
      Integer vert;
      for(int i = 0; i < dMesh.numFaces(); i++){
        line = ("f ");
        f = dMesh.getFace(i);
        for(int j = 0; j < f.size(); j++){
          vert = f.verts.get(j);
          line += str(vert.intValue() + 1);
          if(j < f.size() - 1){
            line += " ";
          }
        }
        
        obj.add(line);
      }
    }
    
    return obj.toArray(new String[obj.size()]);
  }

  void draw(){
    
    if(!show) return;

    strokeWeight(1);
    stroke(0);
    fill(50);
    rect(LLC.x, LLC.y, URC.x, URC.y);
    viewport.beginDraw();
    viewport.camera( cPos.x, cPos.y, cPos.z, 0.0, 0.0, 0.0, 0.0, -1.0, 0.0 );
    viewport.background(50);

    if(grid) drawGrid();

    viewport.fill(175);
    viewport.stroke(0);

    if(wireframe){
      viewport.noFill();
      viewport.stroke(0);
    }
    
    Mesh dMesh = mesh;
    if(showSub){
      dMesh = subMesh;
    }
    
    Tuple f;
    Vertex v;
    
    for(int i = 0; i < dMesh.numFaces(); i++){
      f = dMesh.getFace(i);
      
      viewport.beginShape();
      for(int j = 0; j < f.size(); j++){
        v = dMesh.getVertex(f.verts.get(j));
        viewport.vertex(v.x, v.y, v.z);
      }
      viewport.endShape(CLOSE);
    }
    
    viewport.endDraw();
    image(viewport, int(LLC.x), int(LLC.y));
  }
  
  void clearSurface(){
    mesh.clear();
  }
  
  void handlePush(){
    prev.x = (float(mouseX) - LLC.x) / (URC.x - LLC.x);
    prev.y = (URC.y - LLC.y) - (float(mouseY) - LLC.y);
    prev.y /= (URC.y - LLC.y);
  }
  
  void handleDrag(){
    
    // rotate camera around center point
    if(mouseButton == LEFT){
      current.x = (float(mouseX) - LLC.x) / (URC.x - LLC.x);
      current.y = (URC.y - LLC.y) - (float(mouseY) - LLC.y);
      current.y /= (URC.y - LLC.y);
      
      float theta = atan2(cPos.z, (cPos.x + 0.00001));
      float phi = acos(cPos.y / (r + 0.000001));
      theta += (current.x - prev.x) * 2.0;
      phi += (current.y - prev.y) * 2.0;
      phi = max(min(phi, PI - 0.000001), 0.000001);
      
      cPos.x = r * sin(phi) * cos(theta);
      cPos.y = r * cos(phi);
      cPos.z = r * sin(phi) * sin(theta);
      
      // update previus coordinates
      prev.assign(current);
    }
    
    // zoom in/out
    else if(mouseButton == RIGHT){
      current.x = (float(mouseX) - LLC.x) / (URC.x - LLC.x);
      current.y = (URC.y - LLC.y) - (float(mouseY) - LLC.y);
      current.y /= (URC.y - LLC.y);      
      
      float theta = atan2(cPos.z, (cPos.x + 0.00001));
      float phi = acos(cPos.y / (r + 0.000001));
      r += (current.y - prev.y) * 500.0; 

      cPos.x = r * sin(phi) * cos(theta);
      cPos.y = r * cos(phi);
      cPos.z = r * sin(phi) * sin(theta);

      // update previus coordinates
      prev.assign(current);
    }
  }
}
