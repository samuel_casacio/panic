// Samuel Casacio
// CPSC 881 Geometric Modeling
// CurveCanvas
// A canvas that draws curves

class CurveCanvas extends Viewport{

  // canvas info
  boolean drawControl;
  boolean dirty;
  
  // curve info
  PointData controlPolygon;
  PointData curve;
  CurveType type;
  
  // ux info
  int pointSelected;       // currently selected point
  int pointHover;          // index of the point where mouse is hovering
  int eRadius;             // radius of ellipse
  int iterations;          // number of DCJ interations
  int subdivisions;        // number of subdivisions;
  int bsSamples;           // number of b-spline samples for curve
  int order;               // order of b-spline curve
  
  CurveCanvas(Point llc, Point urc){
    super(llc, urc);
    drawControl = true;
    controlPolygon = new PointData();
    curve = new PointData();
    type = CurveType.BEZDCJ;
    
    pointHover = -1;
    pointSelected = -1;
    eRadius = 10;
    order = 3;
    iterations = 1;
    subdivisions = 1;
    bsSamples = 3;
  }
  
  void clear(){
    controlPolygon.clear();
    curve.clear();
    pointSelected = -1;
    pointHover = -1;
    order = 3;
  }
  
  void deleteVertex(){
    if(pointSelected != -1){
      controlPolygon.remove(pointSelected);
      pointSelected = max(pointSelected - 1, 0);
      if(controlPolygon.size() == 0){
        pointSelected = -1;
      }
      dirty = true;
    }
  }
  
  Point getSelected(){
    if(pointSelected != -1){
      return controlPolygon.get(pointSelected);
    }
    return (new Point(0.0, 0.0));
  }
  
  PointData getCurve(){
    return curve;
  }
  
  void setClosed(boolean closed){
    dirty = true;
    this.controlPolygon.setClosed(closed);
  }
  
  boolean getClosed(){
    return this.controlPolygon.closed;
  }
  
  void setIterations(int iterations){
    dirty = true;
    this.iterations = iterations;
  }
  
  void setSubdivisions(int subdivisions){
    dirty = true;
    this.subdivisions = subdivisions;
  }
  
  void setBsSamples(int samples){
    dirty = true;
    this.bsSamples = samples;
  }
  
  void setOrder(int order){
    dirty = true;
    this.order = max(order, 3);
  }
  
  void drawControlPolygon(boolean set){
    drawControl = set;
  }
  
  void changeType(CurveType type){
    dirty = true;
    this.type = type;
  }
  
  CurveType getType(){
    return type;
  }
  
  int size(){
    return controlPolygon.size();
  }

  boolean isDirty(){
    return dirty;
  }

  void updateCurve(){
    if(dirty){
      dirty = false;
      if(controlPolygon.size() == 0) return;

      PointData temp = controlPolygon.deepCopy();

      if(controlPolygon.closed){
        temp.add(controlPolygon.get(0));
      }
      
      switch(type.value){

        // bezier de Casteljau
        case 0:
          curve = deCasteljau(temp, iterations);
          break;
          
        // bezier subdivision
        case 1:
          curve = bezierSD(temp, 0.5, subdivisions);
          break;
        
        // b-spline
        case 2:
          curve = bSpline(temp, order, bsSamples);
          break;
      }

      curve.closed = controlPolygon.closed;
      if(curve.size() > 0){
        if(!curve.closed && !controlPolygon.last().equals(curve.last())){
          curve.add(new Point(controlPolygon.last()));
        }
        
        else if(curve.closed && !controlPolygon.first().equals(curve.last())){
          curve.add(new Point(controlPolygon.first()));
        }
      }
    }
  }
  
  void draw(){

    if(!show) return;
    
    updateCurve();
    
    strokeWeight(1);
    stroke(0);
    fill(200);
    rect(LLC.x, LLC.y, URC.x, URC.y);

    Point p;
    
    // draw the control polygon
    if(drawControl){
      noFill();
      stroke(150);
      beginShape();
      for(int i = 0; i < controlPolygon.size(); i++){
        p = controlPolygon.get(i);
        vertex(int(p.x), int(p.y));
      }
      endShape();    
    }
    
    // draw the control polygon verticies
    for(int i = 0; i < controlPolygon.size(); i++){
      p = controlPolygon.get(i);
      if(pointHover != i && pointSelected != i && i != controlPolygon.size() - 1 && i > 0){
        fill(0,0,0);
      }
      else if(pointSelected == i){
        fill(155, 20, 0);
      }
      else if(pointHover == i){
        fill(0,155,20);
      }
      else if(i == controlPolygon.size() - 1){
        fill(50, 20, 250);
      }
      else if(i == 0){
        fill(150, 150, 0);
      }
      ellipse(int(p.x), int(p.y), eRadius, eRadius);
    }
    
    // draw the curve itself
    if(curve.size() > 0){
      noFill();
      stroke(0);
      strokeWeight(3);
      beginShape();
      for(int i = 0; i < curve.size(); i++){
        p = curve.get(i);
        vertex(int(p.x), int(p.y));
      }
      endShape();
    }
    
    strokeWeight(1);
  }
  
  void handleMove(){
    pointHover = controlPolygon.get(float(mouseX), float(mouseY), float(eRadius));
  }
  
  void handlePush(){

    // switch selected point 
    if(pointHover != -1){
      pointSelected = pointHover;
    }
    
    // lay down a new point
    else{
      pointSelected++;
      float x = float(min(max(mouseX, int(LLC.x) + eRadius), int(URC.x) - eRadius));
      float y = float(min(max(mouseY, int(LLC.y) + eRadius), int(URC.y) - eRadius));
      if(controlPolygon.size() == 0){
        controlPolygon.add(new Point(x, y));
      }
      else{
        controlPolygon.add(new Point(x, y), pointSelected);
      }
      dirty = true;
    }
  }
  
  void handleDrag(){
    if(pointSelected == -1) return;
    
    float x = float(min(max(mouseX, int(LLC.x) + eRadius), int(URC.x) - eRadius));
    float y = float(min(max(mouseY, int(LLC.y) + eRadius), int(URC.y) - eRadius));
    Point p = controlPolygon.get(pointSelected);
    p.x = x;
    p.y = y;
    dirty = true;
  }
}
