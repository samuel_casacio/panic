// Samuel Casacio
// CPSC 881 Geometric Modeling
// Panel
// A panel

class Panel{
  Point LLC;  // lower left corner
  Point URC;  // upper right corner
  boolean horizontal;  // is the panel a horizontal panel?
  
  HashMap<String, PanelItem> items;
  ArrayList<PanelItem> itemList;
  
  Panel(Point LLC, Point URC, boolean horz){
    this.items = new HashMap<String, PanelItem>();
    this.itemList = new ArrayList<PanelItem>();
    this.LLC = LLC;
    this.URC = URC;
    this.horizontal = horz;
  }
  
  void setHorizontal(boolean horz){
    horizontal = horz;
  }
  
  // draw all the items in the panel
  void draw(){
    int offset = 0;
    fill(255);
    stroke(0);
    rect(LLC.x, URC.y, URC.x - LLC.x, LLC.y - URC.y);
    noStroke();
    PanelItem item;
    for(int i = 0; i < itemList.size(); i++){
      item = itemList.get(i);
      if(item.isVisible()){
        item.setOffset(offset);
        item.draw();
      }
      else{
        offset++;
      }
    }
  }
  
  PanelItem newItem(ItemType type, String name, boolean visible){
    Point x = new Point(LLC.x, URC.x);
    Point y = new Point(items.size() * 40 + LLC.y, items.size() * 40 + LLC.y + 40);
    if(horizontal){
      x.x = LLC.x + items.size() * 150;
      x.y = x.x + 150;
      y.x = LLC.y;
      y.y = URC.y;
    }
    PanelItem item = null;
    switch(type.value){
      // BUTTON
      case 0:
        item = new Button(name, new Point(x.x, y.x), new Point(x.y, y.y), visible, horizontal);
        break;
      
      // SLIDER
      case 1:
        item = new Slider(name, new Point(x.x, y.x), new Point(x.y, y.y), visible, horizontal);
        break;
      
      // DATA
      case 2:
        item = new PanelItem(name, new Point(x.x, y.x), new Point(x.y, y.y), visible, horizontal);
        break;
    }
    
    if(item != null){
      items.put(name, item);
      itemList.add(item);
    }
    return item;
  }
  
  PanelItem getItem(String name){
    return items.get(name);
  }
  
  // returns null if no event
  PanelItem getEvent(){
    PanelItem item = null;
    for(PanelItem it : items.values()){
      if(it.event()){
        item = it;
        break;
      }
    }
    return item;
  }
}
