// Samuel Casacio
// CPSC 881 Geometric Modeling
// Loop
// Loop subdivision

// given an edge and a face, get the opposite vertex
// face must be a triangle mesh!
int getOpposite(Tuple face, Tuple edge){
  int vert = 0;
  
  for(int i = 0; i < face.size(); i++){
    if(!edge.contains(face.get(i))){
      vert = face.get(i);
    }
  }
  
  return vert;
}

float loopWeight(float n){

  // from real time rendering text
//  float weight = 3.0 / (n * (n + 2.0));

  if(n == 6.0){
    return (5.0 / 8.0);
  }

  // ((3/8 + 1/4 * cos(2pi / n)) / 8) ^ 2 + 3/8
  float weight = (3.0 + 2.0 * cos(TWO_PI / n));
  weight = weight * weight;
  weight /= 64.0;
  weight += (3.0 / 8.0);

  return weight;
}

Mesh loopRecursive(Mesh mesh){
  
  Mesh subMesh = new Mesh();

  // make sure edges are built
  mesh.buildTables();
  
  // output mesh will have vertices from original mesh
  // in front, followed by edge vertices
  
  // copy the masked vertices over
  for(int v = 0; v < mesh.numVerts(); v++){

    float valence = float(mesh.getEdges(mesh.getVertex(v)).size());
    float weight = loopWeight(valence);

    // alpha_n * V
    Vertex newvert = mesh.getVertex(v).deepCopy();
    newvert.x *= weight;
    newvert.y *= weight;
    newvert.z *= weight;

    // (1 - alpha_n)(1 / n)(sum one-ring)
    ArrayList<Integer> ring = mesh.oneRing(v);
    Vertex vert = new Vertex();
    for(int i = 0; i < ring.size(); i++){
      Vertex pi = mesh.getVertex(ring.get(i));
      vert.x += pi.x;
      vert.y += pi.y;
      vert.z += pi.z;
    }
    vert.x *= ( (1.0 - weight) * (1.0 / valence) );
    vert.y *= ( (1.0 - weight) * (1.0 / valence) );
    vert.z *= ( (1.0 - weight) * (1.0 / valence) );
    
    newvert.x += vert.x;
    newvert.y += vert.y;
    newvert.z += vert.z;

    subMesh.addVertex(newvert);
  }

  // for each edge in the mesh, add the centroid
  for(int e = 0; e < mesh.edges.size(); e++){
    
    Tuple edge = mesh.getEdge(e);
    Vertex newvert = mesh.edgeCentroid(e);

    // get faces containing edge
    ArrayList<Integer> faces = mesh.getFaces(edge);

/*    
    // boundary, just add the edge center
    if(faces.size() == 1){
      newvert = mesh.edgeCentroid(e);

      Tuple face = mesh.faces.get(faces.get(0));
      Vertex v1 = mesh.getVertex(edge.get(0));
      Vertex v2 = mesh.getVertex(edge.get(1));
      Vertex v3 = mesh.getVertex(getOpposite(face, edge));
      
      newvert.x = (10.0/24.0)*v1.x + (10.0/24.0)*v2.x + (4.0/24.0)*v3.x;
      newvert.y = (10.0/24.0)*v1.y + (10.0/24.0)*v2.y + (4.0/24.0)*v3.y;
      newvert.z = (10.0/24.0)*v1.z + (10.0/24.0)*v2.z + (4.0/24.0)*v3.z;
    }
*/    
    // normal
    if(faces.size() == 2){
      Tuple face1 = mesh.faces.get(faces.get(0));
      Tuple face2 = mesh.faces.get(faces.get(1));
      Vertex v1 = mesh.getVertex(edge.get(0));
      Vertex v2 = mesh.getVertex(edge.get(1));
      Vertex v3 = mesh.getVertex(getOpposite(face1, edge));
      Vertex v4 = mesh.getVertex(getOpposite(face2, edge));
      
      newvert.x = (3.0*v1.x + 3.0*v2.x + v3.x + v4.x) / 8.0;
      newvert.y = (3.0*v1.y + 3.0*v2.y + v3.y + v4.y) / 8.0;
      newvert.z = (3.0*v1.z + 3.0*v2.z + v3.z + v4.z) / 8.0;
    }

    // copy the vertex to the list

    subMesh.addVertex(newvert);
  }
  
  // rebuild the faces
  int width = mesh.numVerts();
  
  // for each face
  for(int f = 0; f < mesh.numFaces(); f++){
    
    Tuple face = mesh.getFace(f);
    
    // get all 3 centroids
    ArrayList<Integer> edges = mesh.getEdges(face);
    
    int v1 = edges.get(0) + width;
    int v2 = edges.get(1) + width;
    int v3 = edges.get(2) + width;

    Tuple newface = new Tuple();
    newface.add(v1);
    newface.add(v2);
    newface.add(v3);
    subMesh.addFace(newface);
  }

  for(int e = 0; e < mesh.edges.size(); e++){
    Tuple edge = mesh.edges.get(e);
    ArrayList<Integer> efaces = mesh.getFaces(edge);
    
    // ignore rebuilding faces with edges of index < current edge, 
    // it has already been added
    
    for(int f = 0; f < efaces.size(); f++){
      Tuple face = mesh.getFace(efaces.get(f));
      ArrayList<Integer> fedges = mesh.getEdges(face);
      
      // for each edge in the face, build its sub face pair
      for(int i = 0; i < fedges.size(); i++){
        if(fedges.get(i) > e){
          Tuple edge2 = mesh.edges.get(fedges.get(i));
          int shared = edge.get(0);
          if((edge.get(0) != edge2.get(0)) && (edge.get(0) != edge2.get(1))){
            shared = edge.get(1);
          }
          
          int v1 = width + e;
          int v2 = width + fedges.get(i);
          Tuple newface = new Tuple();
          newface.add(v1);
          newface.add(v2);
          newface.add(shared);
          subMesh.addFace(newface);
        }
      }
    }
  }

  return subMesh;
}

Mesh loopSubdivision(Mesh mesh, int iterations){
  
  if(!mesh.isTriangleMesh()){
    return null;
  }
  
  Mesh subMesh = loopRecursive(mesh);
  
  for(int i = 0; i < iterations; i++){
    subMesh = loopRecursive(subMesh);
  }
  
  return subMesh;
}
